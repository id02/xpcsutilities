# Windows installation

- Install Anaconda ( https://www.anaconda.com/products/individual )
- On Anaconda Navigator, left tab "Environments", create a new environment named XPCS
- Select the environment and launch Terminal (Play button)
- Install XPCSUtilities:

```bash
pip install https://gitlab.esrf.fr/id02/xpcsutilities/-/archive/main/xpcsutilities-main.zip
```

- All done! XPCSUtilities has been installed here:

```
C:\Users\<USERNAME>\Anaconda3\envs\XPCS\Scripts
```

- You can start the program by typing `xpcsutilities` on the terminal.

## Update

- Just run again the pip command:

```bash
pip install https://gitlab.esrf.fr/id02/xpcsutilities/-/archive/main/xpcsutilities-main.zip
```
