# -*- coding: utf-8 -*-


default_dpi = 120
default_legend_font_size = 9
default_axis_font_size = 9
default_title_font_size = 9

